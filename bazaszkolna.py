import sys

ALLOWED_TYPES = ('tutor', 'teacher', 'student', 'end')

# {"class name": {"tutor": "" , "students": []},
#  "tutor": "class name",
#  "teacher": {"subject": "", "classes": []},
#  "student": "class_name"}
school = {
    'classes': {},
    'tutors': {},
    'teachers': {},
    'students': {}
}

phrase = sys.argv[1]

while True:
    sort = input('Podaj typ użytkownika: ')
    if sort not in ALLOWED_TYPES:
        print(f'Zły typ użytkownika. Dostępne typy: {ALLOWED_TYPES}')
        continue
    if sort == 'student':
        student_full_name = input('Imię i nazwisko ucznia: ')
        group = input('Podaj nazwę klasy: ')
        if school['classes'].get(group):
            school['classes'][group]['students'].append(student_full_name)
        else:
            school['classes'][group] = {'tutor': '', 'students': [student_full_name]}
        school['students'][student_full_name] = group
    elif sort == 'teacher':
        teacher_full_name = input('Imię i nazwisko nauczyciela: ')
        subject = input('Podaj nazwę przedmiotu: ')
        while True:
            group = input('Podaj nazwę klasy: ')
            if not group:
                break
            if group:
                if not school['teachers'].get(teacher_full_name):
                    school['teachers'][teacher_full_name] = {
                        'subject': subject,
                        'classes': [group]
                    }
                else:
                    school['teachers'][teacher_full_name]['classes'].append(group)
    elif sort == 'tutor':
        tutor_full_name = input('Imię i nazwisko wychowawcy: ')
        group = input('Podaj nazwę klsy: ')
        if school['classes'].get(group):
            school['classes'][group]['tutor'] = tutor_full_name
        else:
            school['classes'][group] = {'tutor': tutor_full_name, 'students': []}
        school['tutors'][tutor_full_name] = group
    elif sort == 'end':
        break
print(school)

if phrase == 'class_name':
    class_name = input('Podaj nazwę klasy: ')
    if school['classes'].get(class_name):
        print(school['classes'][class_name])
    else:
        print('Nie ma takiej klasy!')
if phrase == 'tutor':
    tutor = input('Podaj imię i nazwisko wychowawcy: ')
    if school['tutors'].get(tutor):
        tutor_class = school['tutors'][tutor]
        print(school['classes'][tutor_class]['students'])
if phrase == 'teacher':
    teacher = input('Podaj imię i nazwisko nauczyciela: ')
    if school['teachers'].get(teacher):
        for classname in school['teachers'][teacher]['classes']:
            if school['classes'].get(classname):
                print(school['classes'][classname]['tutor'])
if phrase == 'student':
    student = input('Podaj imię i nazwisko ucznia: ')
    class_name = school['students'][student]
    for teacher_name, data in school['teachers'].items():
        if class_name in data['classes']:
            print(teacher_name, data['subject'])